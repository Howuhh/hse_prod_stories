import os
import gym
import torch
import random
import numpy as np

from collections import deque


class DotDict(dict):
    """dot.notation access to dictionary attributes"""
    def __getattr__(self, attr):
        return self.get(attr)
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __getstate__(self):
        return self

    def __setstate__(self, state):
        self.update(state)
        self.__dict__ = self


class FrameStackWrapper(gym.Wrapper):
    def __init__(self, env, stack=4):
        super().__init__(env)
        self.stack = stack
        self.frame_stack = deque([], maxlen=stack)

    def _stack_to_obs(self):
        return np.concatenate(self.frame_stack, axis=2)

    def reset(self):
        obs = self.env.reset()
        for _ in range(self.stack):
            self.frame_stack.append(obs)
        return self._stack_to_obs()

    def step(self, action):
        obs, reward, done, info = self.env.step(action)
        self.frame_stack.append(obs)
        return self._stack_to_obs(), reward, done, info


def set_seed(env=None, seed=0):
    os.environ["PYTHONHASHSEED"] = str(seed)
    if env is not None:
        env.seed(seed)
        env.action_space.seed(seed)
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)


def rollout(env, agent=None, eval_mode=False):
    state, done = env.reset(), False

    total_reward, total_steps = 0, 0
    while not done:
        if agent is None:
            action = env.action_space.sample()
        else:
            action = agent.act(state.transpose(2, 0, 1)[None], eval_mode=eval_mode)
        state, reward, done, info = env.step(action)
        total_reward += reward
        total_steps += 1

    return total_reward, total_steps


def device(force_cpu=False):
    return "cuda:1" if torch.cuda.is_available() and not force_cpu else "cpu"
