import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.distributions import Categorical

from copy import deepcopy
from sac.utils import device


class Actor(nn.Module):
    def __init__(self, state_size, action_size):
        super().__init__()
        self.actor = nn.Sequential(
            nn.Conv2d(state_size, 32, kernel_size=3, stride=2, padding=1), nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=3, stride=2, padding=1), nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=2, padding=1), nn.ReLU(),
            nn.Flatten(), nn.Linear(256, action_size)
        )

    def forward(self, state, eval_mode=False, return_probs=False):
        logits = self.actor(state)
        probs = F.softmax(logits, dim=-1)

        policy_dist = Categorical(probs=probs)
        if eval_mode:
            action = torch.argmax(probs, dim=-1)
        else:
            action = policy_dist.sample()

        if return_probs:
            log_probs = F.log_softmax(logits, dim=-1)
            return action, probs, log_probs

        return action


class Critic(nn.Module):
    def __init__(self, state_size, action_size):
        super().__init__()
        self.critic = nn.Sequential(
            nn.Conv2d(state_size, 32, kernel_size=3, stride=2, padding=1), nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=3, stride=2, padding=1), nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=2, padding=1), nn.ReLU(),
            nn.Flatten(), nn.Linear(256, action_size)
        )

    def forward(self, state):
        return self.critic(state)


class SoftActorCritic(nn.Module):
    def __init__(self, state_size, action_size, gamma=0.99, tau=0.005, alpha=0.1,
                 actor_lr=1e-4, critic_lr=1e-4):
        super().__init__()
        self.actor = Actor(state_size, action_size).to(device())
        self.actor_optimizer = optim.Adam(self.actor.parameters(), lr=actor_lr)

        self.critic1 = Critic(state_size, action_size).to(device())
        self.critic1_optimizer = optim.Adam(self.critic1.parameters(), lr=critic_lr)

        self.critic2 = Critic(state_size, action_size).to(device())
        self.critic2_optimizer = optim.Adam(self.critic2.parameters(), lr=critic_lr)

        with torch.no_grad():
            self.target_critic1 = deepcopy(self.critic1)
            self.target_critic2 = deepcopy(self.critic2)

        self.tau = tau
        self.gamma = gamma
        self._alpha = alpha
        self.alpha = alpha

    def _soft_update(self, target, source):
        for tp, sp in zip(target.parameters(), source.parameters()):
            tp.data.copy_((1 - self.tau) * tp.data + self.tau * sp.data)

    def _actor_loss(self, state):
        action, action_probs, action_log_probs = self.actor(state, return_probs=True)

        Q_target = torch.min(
            self.critic1(state),
            self.critic2(state)
        )
        # True expectation over all actions + sample estimate for expectation over states
        loss = ((self.alpha * action_log_probs - Q_target.detach()) * action_probs).sum(dim=1).mean()

        assert action_log_probs.shape == Q_target.shape == action_probs.shape

        return loss

    def _critic_loss(self, state, action, reward, next_state, done):
        with torch.no_grad():
            next_action, next_action_probs, next_action_log_probs = self.actor(next_state, return_probs=True)

            Q_min = torch.min(
                self.target_critic1(next_state),
                self.target_critic2(next_state)
            )
            # True expectation over actions to estimate V(s')
            Q_next = (next_action_probs * (Q_min - self.alpha * next_action_log_probs)).sum(dim=1)
            Q_target = reward + self.gamma * (1 - done) * Q_next

            assert Q_next.shape == reward.shape
            assert next_action_probs.shape == Q_min.shape == next_action_log_probs.shape

        # NOTE: gather need (batch_size, 1) action shape, not (batch_size,)
        Q1 = self.critic1(state).gather(1, action.reshape(-1, 1).long()).view(-1)
        Q2 = self.critic2(state).gather(1, action.reshape(-1, 1).long()).view(-1)

        loss = F.mse_loss(Q1, Q_target) + F.mse_loss(Q2, Q_target)

        assert Q1.shape == Q_target.shape and Q2.shape == Q_target.shape

        return loss
    
    def update(self, batch):
        state, action, reward, next_state, done = batch

        # Critic1 & Critic2 update
        critic_losses = self._critic_loss(state, action, reward, next_state, done)

        self.critic1_optimizer.zero_grad()
        self.critic2_optimizer.zero_grad()
        critic_losses.backward()
        self.critic1_optimizer.step()
        self.critic2_optimizer.step()

        # Actor update    
        actor_loss = self._actor_loss(state)

        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        #  Target networks soft update
        with torch.no_grad():
            self._soft_update(self.target_critic1, self.critic1)
            self._soft_update(self.target_critic2, self.critic2)

        return actor_loss.item(), critic_losses.item()

    def act(self, state, eval_mode=False):
        with torch.no_grad():
            state = torch.tensor(state, device=device(), dtype=torch.float32)
            action = self.actor(state, eval_mode=eval_mode).cpu().numpy().item()
        return action

    def update_alpha(self, t, timesteps):
        self.alpha = max(0, self._alpha - (self._alpha * t) / timesteps)
