import os
import torch
import wandb
import numpy as np

from copy import deepcopy
from tqdm.auto import trange
from mapgen import VideoRecorder
from sac.utils import set_seed, rollout


class SACTrainer:
    def __init__(self, env, train_config):
        self.env = env
        self.eval_env = deepcopy(env)
        self.train_config = train_config
        os.makedirs(train_config.checkpoint_path, exist_ok=True)

    def train(self, agent, buffer):
        config = self.train_config

        print("Training seed:", config.seed)
        set_seed(self.env, config.seed)

        state, done = self.env.reset(), False
        for t in trange(1, config.timesteps + 1, desc="Training"):
            if done:
                state, done = self.env.reset(), False

            if t < config.start_train:
                action = self.env.action_space.sample()
            else:
                action = agent.act(state.transpose(2, 0, 1)[None])

            next_state, reward, done, _ = self.env.step(action)

            buffer.add((state.transpose(2, 0, 1), action, reward, next_state.transpose(2, 0, 1), done))
            state = next_state

            if t >= config.start_train:
                if t % config.update_every == 0:
                    for _ in range(config.update_every):
                        batch = buffer.sample(batch_size=config.batch_size)
                        actor_loss, critic_loss = agent.update(batch)
                        
                        # agent.update_alpha(t, config.timesteps)

                        # if t % config.log_every == 0:
                        wandb.log({
                            "train/actor_loss": actor_loss,
                            "train/critic_loss": critic_loss,
                            "train/alpha": agent.alpha,
                            "step": t
                        })

                if t % config.eval_every == 0:
                    reward_mean, reward_std, steps_mean, steps_std = self.eval(agent, config.eval_episodes, config.seed)
                    wandb.log({
                        "eval/reward_mean": reward_mean,
                        "eval/reward_std": reward_std,
                        "eval/steps_mean": steps_mean,
                        "eval/steps_std": steps_std,
                        "step": t
                    })
                    # record videos
                    record_env = VideoRecorder(self.eval_env, video_path=os.path.join(config.checkpoint_path, str(t)), fps=64)
                    self.eval_env.seed(32)
                    rollout(record_env, agent, eval_mode=True)
                    wandb.log({"video": wandb.Video(np.array(record_env._frames).transpose(0, 3, 1, 2), fps=32, format="mp4")})

                if t % config.save_every == 0:
                    torch.save(agent.state_dict(), os.path.join(config.checkpoint_path, f"agent_step{t}.pt"))
                    wandb.save(os.path.join(config.checkpoint_path, f"agent_step{t}.pt"))

    def eval(self, agent, eval_episodes, seed):
        set_seed(self.eval_env, seed)
        rewards, steps = zip(*[rollout(self.eval_env, agent, eval_mode=True) for _ in trange(eval_episodes, desc="Evaluation", leave=False)])
        return np.mean(rewards), np.std(rewards), np.mean(steps), np.std(steps)

