import sqlite3
import datetime

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set()

from tslearn.utils import to_time_series_dataset
from tslearn.preprocessing import TimeSeriesResampler


def drop_duplicate_deals(df):
    unique_deals_index = df.sort_values("time")["deal_id"].drop_duplicates().index
    return df.loc[unique_deals_index]

def normalize_session_col(df, target_col):
    df[target_col] = df.groupby("session_id")[target_col].transform(lambda x: (x - x.mean()) / x.std()).fillna(0)
    return df

def split_by_series(df, target_col, count_thr=0, resample=False):
    sessions = []
    
    for _, ts in df[["session_id", target_col]].groupby("session_id"):
        if len(ts[target_col]) > count_thr:
            sessions.append(ts[target_col].tolist())

    sessions = to_time_series_dataset(sessions)

    if resample:
        sessions = TimeSeriesResampler(sz=sessions.shape[1]).transform(sessions)
            
    return sessions

def plot_clusters(X, y, n_clusters, estimator=None, title="Clustering"):
    plt.figure(figsize=(20, 15))

    n = int(np.ceil(np.sqrt(n_clusters)))
    
    for yi in range(n_clusters):
        plt.subplot(n, n, yi + 1)
        
        for xx in X[y == yi]:
            plt.plot(xx.ravel(), "k-", alpha=.2)
        
        if estimator is not None:
            plt.plot(estimator.cluster_centers_[yi].ravel(), "r-")
        
        plt.text(0.55, 0.85,'Cluster %d' % (yi + 1), transform=plt.gca().transAxes)
        
        if yi == 1:
            plt.title(title)


def load_dataset(data_path):
    con = sqlite3.connect(data_path)

    trading_session = pd.read_sql('SELECT * FROM Trading_session', con)
    trading_session = trading_session[trading_session["trading_type"] == "monthly"]
    chart_data = drop_duplicate_deals(pd.read_sql('SELECT * FROM Chart_data', con))

    full_df = chart_data.join(trading_session.set_index("id"), on="session_id", how="inner")

    full_df["datetime"] = pd.to_datetime(full_df.date + " " + full_df.time)
    full_df.drop(["time", "date", "id", "deal_id", "trading_type"], axis=1, inplace=True)
    full_df.sort_values(["session_id", "datetime"], inplace=True)

    return full_df