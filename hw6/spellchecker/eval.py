from tqdm.auto import tqdm
from .check import SpellChecker
from collections import defaultdict


def evaluate(checker: SpellChecker, test_data: list[list[str]], top_k: list[int] = (1, 3, 5, 10)) -> tuple[dict[int, float], float]:
    topk_acc = defaultdict(lambda: 0)
    mean_place = []

    for word, pred_word in tqdm(test_data, desc="Evaluation"):
        corrections = checker.suggest_correction(word)

        for k in top_k:
            topk_acc[k] += (pred_word in corrections[:k])

        place = None if pred_word not in corrections else corrections.index(pred_word) + 1
        if place is not None:
            mean_place.append(place) # не совсем честно, что только для не None

    for k in top_k:
        topk_acc[k] /= len(test_data)

    return topk_acc, sum(mean_place) / len(test_data)