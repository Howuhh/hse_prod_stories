import os
import argparse
import numpy as np
from scipy.stats import rankdata

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', metavar="input", default='./in.txt', type=str, help='Input file path')
    parser.add_argument('output', metavar="output", default='./out.txt', type=str, help='Output file path')
    args = parser.parse_args()

    return args


def conjugate_test(x: np.ndarray, y: np.ndarray):
    if len(x) < 9:
        raise RuntimeError(f"Number of points should be >= 9 for correct results. Got: N={str(len(x))}")

    p = int(round(len(x) / 3))

    y = y[np.argsort(x)]
    ranks = rankdata(y, method="average")

    r1, r2 = ranks[:p].sum(), ranks[-p:].sum()

    rdiff = int(round(r2 - r1))
    rstd = int(round((len(x) + 0.5) * np.sqrt(p / 6)))
    conjugate = round((r2 - r1) / (p * (len(x) - p)), 2)

    return rdiff, rstd, conjugate


def main():
    args = parse_args()

    if not os.path.exists(args.input):
        raise FileNotFoundError(f"No such file: {args.input}")

    with open(args.input, "r") as f:
        x, y = zip(*[map(float, s.split()) for s in f.readlines()])

    diff, std, conj = conjugate_test(np.array(x), np.array(y))

    with open(args.output, "w") as o:
        o.write(f"{diff} {std} {conj}\n")


if __name__ == "__main__":
    main()