import torch
import numpy as np

from sac.utils import device


class ReplayBuffer:
    def __init__(self, buffer_size, state_channels=3):
        # state, action, reward, next_state, done, demo
        # env is very small, so all data on GPU memory
        self.state = torch.empty(buffer_size, state_channels, 11, 11, dtype=torch.float, device=device())
        self.action = torch.empty(buffer_size, dtype=torch.float, device=device())
        self.reward = torch.empty(buffer_size, dtype=torch.float, device=device())
        self.next_state = torch.empty(buffer_size, state_channels, 11, 11, dtype=torch.float, device=device())
        self.done = torch.empty(buffer_size, dtype=torch.int, device=device())

        self.real_size = 0
        self.count = 0
        self.size = buffer_size

    def add(self, transition):
        state, action, reward, next_state, done = transition

        # store transition in the buffer
        self.state[self.count] = torch.as_tensor(state)
        self.action[self.count] = torch.as_tensor(action)
        self.reward[self.count] = torch.as_tensor(reward)
        self.next_state[self.count] = torch.as_tensor(next_state)
        self.done[self.count] = torch.as_tensor(done)

        # update real size
        self.count = (self.count + 1) % self.size
        self.real_size = min(self.size, self.real_size + 1)

    def sample(self, batch_size):
        assert self.real_size >= batch_size

        sample_idxs = np.random.randint(0, self.real_size, size=batch_size)

        batch = (
            self.state[sample_idxs],
            self.action[sample_idxs],
            self.reward[sample_idxs],
            self.next_state[sample_idxs],
            self.done[sample_idxs],
        )
        return batch

