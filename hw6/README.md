# Simple spellchecker

Uses only one distance: Damerau–Levenshtein distance

Test data from: http://aspell.net/test/cur/
English words frequency from: https://www.kaggle.com/rtatman/english-word-frequency

# Metrics
Mean place of correct word in suggestions: 1.38

**ACC@1**: 0.55   
**ACC@3**: 0.67   
**ACC@5**: 0.70  
