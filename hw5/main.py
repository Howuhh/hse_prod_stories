import yaml
import wandb
from datetime import datetime
from pprint import pprint

from sac.agent import SoftActorCritic
from sac.utils import DotDict, FrameStackWrapper
from sac.trainer import SACTrainer
from sac.buffer import ReplayBuffer

from mapgen import Dungeon


def start_run(config_path):
    now = datetime.now()
    wandb.init(
        project="MapGen",
        name=f'baseline:{now.hour}:{now.minute}:{now.second}-{now.day}.{now.month}.{now.year}',
        group="mapgen_sac",
        mode="online",
        entity="Howuhh",
        reinit=True
    )
    with open(config_path, "r") as f:
        config = DotDict(yaml.safe_load(f))
    pprint(config)

    env = Dungeon(**config.env_config)
    env = FrameStackWrapper(env, stack=3)

    buffer = ReplayBuffer(**config.buffer_config)
    agent = SoftActorCritic(**config.agent_config)

    trainer = SACTrainer(env, DotDict(config.trainer_config))
    trainer.train(agent, buffer)

    wandb.finish()


def main():
    configs = [
        "configs/sac_base.yaml"
    ]
    for config in configs:
        start_run(config)


if __name__ == "__main__":
    main()
