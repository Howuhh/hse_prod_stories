import csv
from collections import defaultdict


def read_word_freq(path: str) -> defaultdict[str, float]:
    word_freq, total_count = defaultdict(lambda: 0.0), 0

    with open(path, "r") as f:
        reader = csv.reader(f, delimiter=",")
        next(reader)
        for word, freq in reader:
            word_freq[word] = int(freq)
            total_count += int(freq)

    for key in word_freq.keys():
        word_freq[key] = word_freq[key] / total_count

    return word_freq


if __name__ == "__main__":
    read_word_freq("../data/unigram_freq.csv")