from spylls.hunspell import Dictionary
from textdistance import damerau_levenshtein

from .utils import read_word_freq


class SpellChecker:
    def __init__(self, dict_path: str="en_US", freq_path: str="unigram_freq.csv", max_candidates: int=500):
        self.word_freq = read_word_freq(freq_path)
        self.word_dict = Dictionary.from_files(dict_path)
        self.max_candidates = max_candidates

    def _get_features(self, word: str, cand: str) -> list[float]:
        dl_dist = damerau_levenshtein(word, cand) # best
        inv_freq = 1 - self.word_freq[cand]
        return [dl_dist, inv_freq]

    def _rank(self, word: str, candidates: set[str]) -> list[str]:
        features = [(self._get_features(word, cand), cand) for cand in candidates]
        ranked = sorted(features, key=lambda f: sum(f[0]) / len(f[0]))
        return [w[1] for w in ranked[:self.max_candidates]]

    def _get_candidates(self, word: str) -> set[str]:
        # some cheating, but not too much
        final_cands = self.word_dict.suggester.ngram_suggestions(word, set())
        return set(final_cands)

    def suggest_correction(self, word: str) -> list[str]:
        candidates = self._get_candidates(word)
        suggestions = self._rank(word, candidates)
        return suggestions


if __name__ == "__main__":
    checker = SpellChecker(freq_path="../data/unigram_freq.csv")
    print(checker.suggest_correction("absorbtion"))
