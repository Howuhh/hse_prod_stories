from spellchecker.eval import evaluate
from spellchecker.check import SpellChecker


def main():
    checker = SpellChecker(freq_path="data/unigram_freq.csv")
    with open("data/test_data.tab", "r") as f:
        test_data = [line.strip().split("\t") for line in f.readlines()]

        acc, mean_place = evaluate(checker, test_data, top_k=[1, 3, 5])

        print(f"Mean place: {mean_place}")
        for k, v in acc.items():
            print(f"ACC@{k}: {v}")


if __name__ == "__main__":
    main()